const usersUrl = 'https://ajax.test-danit.com/api/json/users';
const postsUrl = 'https://ajax.test-danit.com/api/json/posts';
const container = document.querySelector('.main');
class Card { /*making class Card & render method*/
  constructor(postUserId, userName, email, title, body, userId, postId) {
    this.postUserId = postUserId; this.userName = userName; this.email = email;
    this.title = title; this.body = body; this.userId = userId; this.postId = postId;}
  render(container) {
    container.insertAdjacentHTML(
      "beforeend", `<div class="card user-card${this.postId}"><p class="name">${this.userName}</p>
        <p class="email">${this.email}</p><p class="title">${this.title}</p><p class="body-text">${this.body}</p>
        <p class="postid">Post id:  ${this.postId}</p><div class="delete" id=${this.postId}>&#9760;</div></div>`)}}
container.innerHTML = '<div class="loading">LOADING...</div>'; /*the engine for preloader*/   
fetch(usersUrl) /*making the first request about the users data*/
  .then((res) => res.json())
  .then((userInfo) => {
    const users = userInfo.map(({ id: postUserId, name: userName, email }) => ({ postUserId, userName, email, }));  /*making the array with users details*/
  fetch(postsUrl).then((res) => res.json()).then((posts) => {/*making the second request about the posts data*/
    const actualPosts = posts.map(({ title, body, userId, id: postId }) => ({/*making the global array which contains the displaying data*/
        title, body, userId, postId, postUserId: users[userId - 1].postUserId,
        userName: users[userId - 1].userName, email: users[userId - 1].email,}));
    actualPostsRender(actualPosts); /*displaying the data*/
    document.addEventListener("click", (event) => { /*post - delete listener*/
      if (event.target.classList.contains("delete")) {
        container.innerHTML = '<div class="loading">LOADING...</div>'; /*the engine for preloader*/  
        fetch(`https://ajax.test-danit.com/api/json/posts/${event.target.id}`, { method: "DELETE" }).then((response) => { /*making the (post-delete) request*/
          if (response.ok) { /*checking the server (post-delete) response*/
            container.innerHTML = '<div class="loading">LOADING</div>'; /*the engine for preloader*/  
            actualPosts.splice(actualPosts.findIndex(({ postId }) => postId === Number(event.target.id)), 1); /*modifying the global array*/
            actualPostsRender(actualPosts);/*displaying the data*/
          }}).catch((err) => console.warn(err)).finally(() => actualPostsRender(actualPosts))}});/*checking & fixing the errors*/      
  }).catch((err) => console.warn(err)); /*checking the errors*/ 
}).catch((err) => console.warn(err)); /*checking the errors*/ 
function actualPostsRender(array) {/*the displaying - engine of the posts chain*/
  container.innerHTML = ''; 
    array.forEach(({ postUserId, userName, email, title, body, userId, postId }) => {
        new Card(postUserId, userName, email, title, body, userId, postId).render(container)})}

